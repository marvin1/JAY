'use strict';

angular.module('jayApp')
  .controller('SoundcloudCtrl', function ($scope, $http) {
/*    $http.get('/api/awesomeThings').success(function(awesomeThings) {
      $scope.awesomeThings = awesomeThings;
    });*/

    SC.initialize({
      client_id: "aa760cbf2bbb3dba86b5f92b9d56d704",
      redirect_uri: "http://jayspeakman.co.uk"
    });
    $('#tracks').bind('click', function(){
      SC.get("/users/21760/tracks", function(tracks){
            $('#track').children().attr('ID','sound').remove();
            var soundsTotal = tracks.length,
                i = 0,
                track,
                $soundHolder;
            while (i<soundsTotal){
              track = tracks[i];
              console.log(track)
              $soundHolder=$('<div class="sound" id="sound'+i+'" >' + track.title + '</div> <div class="play" data-trackid="'+track.id+'">PLAY</div><div class="stop" data-trackid="'+track.id+'">STOP</div>')
              $('#track').append($soundHolder);
              i++;
            }
            
            $('.play').bind('click', function(){
              var trid = $(this).data('trackid');
              SC.stream('/tracks/'+trid, function(sound){
                sound.play();
              });
            })

            $('.stop').bind('click', function(){
              
              var trid = $(this).data('trackid');
              SC.stream('/tracks/'+trid, function(sound){
                console.log(sound);
                sound.pause();
              });
            })
          });
        })

      $('#fav').bind('click', function(){
      SC.get("/users/21760/favorites", function(tracks){
            $('#track').children().attr('ID','sound').remove();
            var soundsTotal = tracks.length,
                i = 0,
                track,
                $soundHolder;
            while (i<soundsTotal){
              track = tracks[i];
              console.log(track)
              $soundHolder=$('<div class="sound" id="sound'+i+'" >' + track.title + '</div> <div class="play" data-trackid="'+track.id+'">PLAY</div><div class="stop" data-trackid="'+track.id+'">STOP</div>')
              $('#track').append($soundHolder);
              i++;
            }
            
            $('.play').bind('click', function(){
              var trid = $(this).data('trackid');
              SC.stream('/tracks/'+trid, function(sound){
                sound.play();
              });
            })

            $('.stop').bind('click', function(){
              
              var trid = $(this).data('trackid');
              SC.stream('/tracks/'+trid, function(sound){
                console.log(sound);
                sound.pause();
              });
            })
          });
        })
    
  });
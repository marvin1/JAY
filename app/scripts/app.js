'use strict';

angular.module('jayApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngAnimate'
])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'partials/main',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'partials/about',
        controller: 'AboutCtrl'
      })
      .when('/soundcloud', {
        templateUrl: 'partials/soundcloud',
        controller: 'SoundcloudCtrl'
      })
      .when('/work', {
        templateUrl: 'partials/work',
        controller: 'WorkCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
    $locationProvider.html5Mode(true);
  });
